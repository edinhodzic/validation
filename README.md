# Validation

We'll be doing validation using the [Arrow library](https://arrow-kt.io/docs/core/).

## Step 1 : data class

```kotlin
data class Person (
    val firstName: String,
    val lastName: String,
    val dateOfBirth: LocalDate?
)
```

Problem:
- we can create invalid `Person` objects:
    ```kotlin
    val badPerson = Person(
        firstName = "",
        lastName = "",
        dateOfBirth = null
    )
    ```

Solution:
- validate as early as possible

## Step 2 : model validation errors with `Either`

```kotlin
fun validate(
    firstName: String,
    lastName: String,
    dateOfBirth: LocalDate?
): Either<String, Person> {
    val firstNameValidated = firstName.right()
        .ensure(
            { "first name cannot be blank" },
            { it.isNotBlank() }
        )

    val lastNameValidated = lastName.right()
        .ensure(
            { "last name must contain at least 2 characters" },
            { it.length >= 2 }
        )

    val dateOfBirthValidated = dateOfBirth.right()
        .ensure(
            { "date of birth must be before today" },
            { it?.let { it.isBefore(LocalDate.now()) } ?: true }
        )

    return firstNameValidated.zip(
        lastNameValidated,
        dateOfBirthValidated,
        ::Person
    )
}
```

Takeaway:
- `Either`s compose with `.zip`
- only when validation passes, does the `::Person` instantiate our data class

Problem:
- `Either` fails fast and consequently we cannot accumulate errors
  ```kotlin
  val badPerson = Person.validate(
      firstName = "",
      lastName = "",
      dateOfBirth = null
  )
  println("bad person  : $badPerson")
  ```
  ```shell
  bad person  : Either.Left(first name cannot be blank)
  ```
  - error reported for `firstName` but not for `lastName` or `dateOfBirth`

Solution:
- model errors with `Validated`

## Step 3 : model validation errors with `Validated`

```kotlin
fun validate(
    firstName: String,
    lastName: String,
    dateOfBirth: LocalDate?
): ValidatedNel<String, Person> {
    val firstNameValidated = firstName.right()
        .ensure(
            { "first name cannot be blank" },
            { it.isNotBlank() }
        ).toValidatedNel()

    val lastNameValidated = lastName.right()
        .ensure(
            { "last name must contain at least 2 characters" },
            { it.length >= 2 }
        ).toValidatedNel()

    val dateOfBirthValidated = dateOfBirth.right()
        .ensure(
            { "date of birth must be before today" },
            { it?.let { it.isBefore(LocalDate.now()) } ?: true }
        ).toValidatedNel()

    return firstNameValidated.zip(
        lastNameValidated,
        dateOfBirthValidated,
        ::Person
    )
}
```

Takeaway:
- `Validated` fails slow and allows us to accumulate errors
  ```kotlin
  val badPerson = Person.validate(
      firstName = "",
      lastName = "",
      dateOfBirth = null
  )
  println("bad person  : $badPerson")
  ```
  ```shell
  bad person  : Validated.Invalid(NonEmptyList(first name cannot be blank, last name must contain at least 2 characters))
  ```
  - error reported for `firstName` and for `lastName` (`dateOfBirth` is optional and we didn't supply it above)


Problem:
- we can still create invalid `Person` objects:
    ```kotlin
    val badPerson = Person(
        firstName = "",
        lastName = "",
        dateOfBirth = null
    )
    ```

Solution:
- Make the primary `Person` constructor private forcing us to always use `Person.validate(..)` to instantiate `Person` objects:
  ```kotlin
  data class Person private constructor(
      val firstName: String,
      val lastName: String,
      val dateOfBirth: LocalDate?
  )
  ```  

The implication here is that we cannot create invalid `Person` objects. Quite rightly it was pointed out that we can with `Person.copy(..)`. We discussed a bunch of solutions and eventually settled on an appropriately named extension function:
```kotlin
fun Person.copySafe(
    firstName: String,
    lastName: String,
    dateOfBirth: LocalDate?
): Validated<NonEmptyList<String>, Person> =
    Person.invoke(firstName, lastName, dateOfBirth)
```
- appropriately named because when we are tempted to `person.copy(..)` our IDE should also show us that `copySafe(..)` is available
- here we're relying on our own discipline, hence the human factor remains as a way to get bad data into our system, but there's only so much we can do
- other options we discussed:
  - don't use a data class - but it gives us so much more so we decided against this approach
  - using value classes, for example instead of `firstName` being of type `String`, we create a `FirstName` value class and push validation into there

# That's all folks!

- validate early
- leverage functional libraries and type classes they provide
- book recommendation is [Conceptual Mathematics](https://www.amazon.co.uk/gp/product/052171916X/)
  - good to get a base understanding of category theory, subsequent adoption of functional libraries should be easier based on this understanding
