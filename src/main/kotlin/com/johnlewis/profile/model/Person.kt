package com.johnlewis.profile.model

import arrow.core.NonEmptyList
import arrow.core.Validated
import arrow.core.ensure
import arrow.core.right
import arrow.core.zip
import java.time.LocalDate

data class Person private constructor(
    val firstName: String,
    val lastName: String,
    val dateOfBirth: LocalDate?
) {

    companion object {

        operator fun invoke(
            firstName: String,
            lastName: String,
            dateOfBirth: LocalDate?
        ): Validated<NonEmptyList<String>, Person> {

            val firstNameValidated = firstName.right()
                .ensure(
                    { "first name must not be empty" },
                    { it.isNotBlank() }
                ).toValidatedNel()

            val lastNameValidated = lastName.right()
                .ensure(
                    { "last name must be at least 2 characters" },
                    { it.length >= 2 }
                ).toValidatedNel()

            val dateOfBirthValidated = dateOfBirth.right()
                .ensure(
                    { "date of birth must not be before now" },
                    { it?.let { it.isBefore(LocalDate.now()) } ?: true }
                ).toValidatedNel()

            return firstNameValidated.zip(
                lastNameValidated,
                dateOfBirthValidated,
                ::Person
            )
        }

    }

}

    fun Person.copySafe(
        firstName: String,
        lastName: String,
        dateOfBirth: LocalDate?
    ): Validated<NonEmptyList<String>, Person> =
        Person.invoke(firstName, lastName, dateOfBirth)

object Main {
    @JvmStatic
    fun main(args: Array<String>) {

        val goodPerson = Person(
            firstName = "John",
            lastName = "Lewis",
            dateOfBirth = LocalDate.now().minusDays(1)
        )
        println("good person : $goodPerson")

        goodPerson.map { validatedPerson ->
            val newPerson = validatedPerson.copy(
                firstName = "",
                lastName = "",
                dateOfBirth = null
            )
            println("####### newPerson : $newPerson")
        }

        val badPerson = Person(
            firstName = "",
            lastName = "",
            dateOfBirth = null
        )
        println("bad person  : $badPerson")

//        val badPerson2 = Person(
//            firstName = "",
//            lastName = "",
//            dateOfBirth = null
//        )
//        println("bad person2  : $badPerson2")

    }
}
