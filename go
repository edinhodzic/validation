#!/bin/bash

export DOCKER_IMAGE_NAME=eu.gcr.io/jl-container-images/customer.profile

_gradle() {
  if hash gradle 2>/dev/null; then
      echo "Using pre-installed Gradle"
      echo "$@"
      gradle "$@"
  else
      echo "Using Gradle Wrapper"
      ./gradlew "$@"
  fi

}
_help() {
  echo -e "Usage:"
  echo -e "  ./go <command>"
  echo -e "Commands:"
  echo -e "  run - build and run app"
  echo -e "  test - run Detekt & unit tests"
  echo -e "  build - build the app"
  echo -e "  datastore-emulator - run a local datastore emulator [requires docker]"
  echo -e "  help - show this help message"
}



_metadata() {
  BUILD_METADATA_FILE="src/main/resources/build.properties"
  touch $BUILD_METADATA_FILE
cat >$BUILD_METADATA_FILE <<END
build.version=${CI_PIPELINE_IID:-unknown}
build.gitRevision=`git rev-parse --short HEAD`
build.machine=`hostname`
build.time=`date`
END

}

_test () {
  echo "Testing Application"
  _metadata
  _gradle clean check
}

_build() {
  echo "Building Application"
  _metadata
  _gradle clean build
}

_package() {
  echo "Packaging Application"
  _gradle jib --info --stacktrace -Pdocker.image.name=${DOCKER_IMAGE_NAME} -Pdocker.image.tags=latest,${CI_COMMIT_SHA:0:8} -Pdocker.auth.username=_json_key -Pdocker.auth.password="${CI_SERVICE_ACCOUNT_KEY_FLEX}"
}

_k8s() {
  kubectl -n customer-profile $@
}

if [[ $1 =~ ^(build|package|metadata|load-test|k8s|smoke-test|contract-test|test)$ ]]; then
  COMMAND=_$1
  shift
  $COMMAND "$@"
else
  _help
  exit 1
fi


